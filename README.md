# Decentralized Library

This is a Node.js application that implements a decentralized library system, where users can own books, lend them to others, borrow books from others, and leave reviews. The application uses PostgreSQL as the database and provides a RESTful API for interacting with the library.

## Features

- Users can create accounts and manage their profiles.
- Users can add books they own to the library.
- Users can mark their books as available for lending.
- Users can search for available books and request to borrow them.
- Users can track their borrowed and lent books.
- Users can leave reviews and ratings for books they've read.
- Users have a reputation score based on their lending/borrowing history and reviews.

## Prerequisites

- Node.js (v12 or later)
- PostgreSQL (v9.6 or later)

## Getting Started

1. Clone the repository:

```
git clone https://gitlab.com/tellmeY/bookmybook
```

2. Install dependencies:

```
cd bookmybook  
npm install
```

3. Set up the PostgreSQL database:

- Dockerfile Present in the directory can be used to create and run postgres container   
- Update the database connection details in `src/config/database.js` if needed.

4. Start the application:

```
node src/app.js
```

The server should now be running on `http://localhost:3000`.

## API Endpoints

The following API endpoints are available:

### Users

- `GET /users`: Get a list of all users.
- `POST /users`: Create a new user.
- `PUT /users/:userId/reputation`: Update a user's reputation score.

### Books

- `GET /books`: Get a list of all books.
- `POST /books`: Add a new book.
- `PUT /books/:bookId/availability`: Update a book's availability for lending.

### Loans

- `GET /loans`: Get a list of all loans.
- `POST /loans`: Create a new loan (borrow a book).
- `PUT /loans/:loanId/return`: Mark a loan as returned.

### Reviews

- `GET /reviews`: Get a list of all reviews.
- `POST /reviews`: Add a new review for a book.

### Wishlist

- `GET /wishlist/:userId`: Get a user's wishlist.
- `POST /wishlist`: Add a book to a user's wishlist.

## API Usage

You can use tools like Postman, curl, or a web browser to interact with the API endpoints. Here are some examples:

### Create a New User

```
curl -X POST -H "Content-Type: application/json" -d '{"username":"johndoe","email":"johndoe@example.com","password":"mypassword"}' http://localhost:3000/users
```

### Add a New Book

```
curl -X POST -H "Content-Type: application/json" -d '{"title":"The Great Gatsby","author":"F. Scott Fitzgerald","genre":"Fiction","ownerId":1}' http://localhost:3000/books
```

### Borrow a Book

```
curl -X POST -H "Content-Type: application/json" -d '{"bookId":1,"borrowerId":2,"lenderId":1,"loanStartDate":"2023-05-01","dueDate":"2023-05-15"}' http://localhost:3000/loans
```

### Leave a Book Review

```
curl -X POST -H "Content-Type: application/json" -d '{"bookId":1,"userId":2,"rating":5,"review":"Great book!"}' http://localhost:3000/reviews
```

## Contributing

Contributions are welcome! If you find any issues or have suggestions for improvement, please open an issue or submit a pull request.

## License

This project is licensed under the [AGPL V3](LICENSE).
