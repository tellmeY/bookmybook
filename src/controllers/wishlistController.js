const db = require('../config/database');

// Get a user's wishlist
const getUserWishlist = async (req, res) => {
  const { userId } = req.params;

  try {
    const result = await db.query(
      'SELECT Books.* FROM Wishlist JOIN Books ON Wishlist.bookId = Books.bookId WHERE Wishlist.userId = $1',
      [userId]
    );
    res.json(result.rows);
  } catch (err) {
    console.error(err);
    res.status(500).json({ error: 'Internal server error' });
  }
};

// Add a book to a user's wishlist
const addToWishlist = async (req, res) => {
  const { userId, bookId } = req.body;

  try {
    const result = await db.query(
      'INSERT INTO Wishlist (userId, bookId) VALUES ($1, $2) RETURNING *',
      [userId, bookId]
    );
    res.status(201).json(result.rows[0]);
  } catch (err) {
    console.error(err);
    res.status(500).json({ error: 'Internal server error' });
  }
};

module.exports = {
  getUserWishlist,
  addToWishlist,
};
