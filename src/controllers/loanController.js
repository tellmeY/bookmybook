const db = require('../config/database');

// Get all loans
const getAllLoans = async (req, res) => {
  try {
    const result = await db.query('SELECT * FROM Loans');
    res.json(result.rows);
  } catch (err) {
    console.error(err);
    res.status(500).json({ error: 'Internal server error' });
  }
};

// Create a new loan
const createLoan = async (req, res) => {
  const { bookId, borrowerId, lenderId, loanStartDate, dueDate } = req.body;

  try {
    const result = await db.query(
      'INSERT INTO Loans (bookId, borrowerId, lenderId, loanStartDate, dueDate) VALUES ($1, $2, $3, $4, $5) RETURNING *',
      [bookId, borrowerId, lenderId, loanStartDate, dueDate]
    );
    res.status(201).json(result.rows[0]);
  } catch (err) {
    console.error(err);
    res.status(500).json({ error: 'Internal server error' });
  }
};

// Mark a loan as returned
const markLoanReturned = async (req, res) => {
  const { loanId } = req.params;

  try {
    const result = await db.query(
      'UPDATE Loans SET returned = true WHERE loanId = $1 RETURNING *',
      [loanId]
    );
    res.json(result.rows[0]);
  } catch (err) {
    console.error(err);
    res.status(500).json({ error: 'Internal server error' });
  }
};

module.exports = {
  getAllLoans,
  createLoan,
  markLoanReturned,
};
