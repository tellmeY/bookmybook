const db = require('../config/database');

// Get all books
const getAllBooks = async (req, res) => {
  try {
    const result = await db.query('SELECT * FROM Books');
    res.json(result.rows);
  } catch (err) {
    console.error(err);
    res.status(500).json({ error: 'Internal server error' });
  }
};

// Create a new book
const createBook = async (req, res) => {
  const { title, author } = req.body;
  const ownerId = req.user && req.user.userId; // Check if req.user exists before accessing userId

  if (!ownerId) {
    console.error('Owner ID is missing or null');
    return res.status(400).json({ error: 'Owner ID is missing or null' });
  }

  try {
    const result = await db.query(
      'INSERT INTO Books (title, author, ownerId) VALUES ($1, $2, $3) RETURNING *',
      [title, author, ownerId]
    );
    res.status(201).json(result.rows[0]);
  } catch (err) {
    console.error(err);
    res.status(500).json({ error: 'Internal server error' });
  }
};

module.exports = {
  createBook,
};
// Update a book's availability
const updateBookAvailability = async (req, res) => {
  const { bookId } = req.params;
  const { available } = req.body;

  try {
    const result = await db.query(
      'UPDATE Books SET available = $1 WHERE bookId = $2 RETURNING *',
      [available, bookId]
    );
    res.json(result.rows[0]);
  } catch (err) {
    console.error(err);
    res.status(500).json({ error: 'Internal server error' });
  }
};

module.exports = {
  getAllBooks,
  createBook,
  updateBookAvailability,
};
