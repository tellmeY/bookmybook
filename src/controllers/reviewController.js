const db = require('../config/database');

// Get all reviews
const getAllReviews = async (req, res) => {
  try {
    const result = await db.query('SELECT * FROM Reviews');
    res.json(result.rows);
  } catch (err) {
    console.error(err);
    res.status(500).json({ error: 'Internal server error' });
  }
};

// Create a new review
const createReview = async (req, res) => {
  const { bookId, userId, rating, review } = req.body;

  try {
    const result = await db.query(
      'INSERT INTO Reviews (bookId, userId, rating, review) VALUES ($1, $2, $3, $4) RETURNING *',
      [bookId, userId, rating, review]
    );
    res.status(201).json(result.rows[0]);
  } catch (err) {
    console.error(err);
    res.status(500).json({ error: 'Internal server error' });
  }
};

module.exports = {
  getAllReviews,
  createReview,
};
