const jwt = require('jsonwebtoken');
const db = require('../config/database');

const Loan = {
  async getAllLoans() {
    const result = await db.query('SELECT * FROM Loans');
    return result.rows;
  },

  async getLoanById(loanId) {
    const result = await db.query('SELECT * FROM Loans WHERE loanId = $1', [loanId]);
    return result.rows[0];
  },

  async createLoan(bookId, borrowerId, lenderId, loanStartDate, dueDate, token) {
    // Verify JWT token
    try {
      const decoded = jwt.verify(token, process.env.JWT_SECRET);
      // Additional verification logic if needed
    } catch (err) {
      throw new Error('Unauthorized: Invalid token');
    }

    const result = await db.query(
      'INSERT INTO Loans (bookId, borrowerId, lenderId, loanStartDate, dueDate) VALUES ($1, $2, $3, $4, $5) RETURNING *',
      [bookId, borrowerId, lenderId, loanStartDate, dueDate]
    );
    return result.rows[0];
  },

  async markLoanReturned(loanId, token) {
    // Verify JWT token
    try {
      const decoded = jwt.verify(token, process.env.JWT_SECRET);
      // Additional verification logic if needed
    } catch (err) {
      throw new Error('Unauthorized: Invalid token');
    }

    const result = await db.query(
      'UPDATE Loans SET returned = true WHERE loanId = $1 RETURNING *',
      [loanId]
    );
    return result.rows[0];
  },
};

module.exports = Loan;
