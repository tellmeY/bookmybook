const jwt = require('jsonwebtoken');
const db = require('../config/database');

const Wishlist = {
  async getUserWishlist(userId) {
    const result = await db.query(
      'SELECT Books.* FROM Wishlist JOIN Books ON Wishlist.bookId = Books.bookId WHERE Wishlist.userId = $1',
      [userId]
    );
    return result.rows;
  },

  async addToWishlist(userId, bookId, token) {
    // Verify JWT token
    try {
      const decoded = jwt.verify(token, process.env.JWT_SECRET);
      // Additional verification logic if needed
    } catch (err) {
      throw new Error('Unauthorized: Invalid token');
    }

    const result = await db.query(
      'INSERT INTO Wishlist (userId, bookId) VALUES ($1, $2) RETURNING *',
      [userId, bookId]
    );
    return result.rows[0];
  },

  async removeFromWishlist(userId, bookId, token) {
    // Verify JWT token
    try {
      const decoded = jwt.verify(token, process.env.JWT_SECRET);
      // Additional verification logic if needed
    } catch (err) {
      throw new Error('Unauthorized: Invalid token');
    }

    const result = await db.query(
      'DELETE FROM Wishlist WHERE userId = $1 AND bookId = $2 RETURNING *',
      [userId, bookId]
    );
    return result.rows[0];
  },
};

module.exports = Wishlist;
