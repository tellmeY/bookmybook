const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
const db = require('../config/database');

const User = {
  async getAllUsers() {
    const result = await db.query('SELECT * FROM Users');
    return result.rows;
  },

  async getUserById(userId) {
    const result = await db.query('SELECT * FROM Users WHERE userId = $1', [userId]);
    return result.rows[0];
  },

  async createUser(username, email, password) {
    const hashedPassword = await bcrypt.hash(password, 10);
    const result = await db.query(
      'INSERT INTO Users (username, email, password) VALUES ($1, $2, $3) RETURNING *',
      [username, email, hashedPassword]
    );
    return result.rows[0];
  },

  async updateUserReputation(userId, reputationScore, token) {
    // Verify JWT token
    try {
      const decoded = jwt.verify(token, process.env.JWT_SECRET);
      // Additional verification logic if needed
      // e.g., check if the decoded userId matches the userId in the request

      const result = await db.query(
        'UPDATE Users SET reputation_score = $1 WHERE userId = $2 RETURNING *',
        [reputationScore, userId]
      );
      return result.rows[0];
    } catch (err) {
      throw new Error('Unauthorized: Invalid token');
    }
  },

  async authenticateUser(email, password) {
    const result = await db.query('SELECT * FROM Users WHERE email = $1', [email]);
    const user = result.rows[0];

    if (!user) {
      throw new Error('Invalid email or password');
    }

    const isValidPassword = await bcrypt.compare(password, user.password);

    if (!isValidPassword) {
      throw new Error('Invalid email or password');
    }

    return user;
  },
};

module.exports = User;
