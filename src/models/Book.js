const jwt = require('jsonwebtoken');
const db = require('../config/database');

const Book = {
  async getAllBooks() {
    const result = await db.query('SELECT * FROM Books');
    return result.rows;
  },

  async getBookById(bookId) {
    const result = await db.query('SELECT * FROM Books WHERE bookId = $1', [bookId]);
    return result.rows[0];
  },

  async createBook(title, author, genre, ownerId, token) {
    // Verify JWT token
    try {
      const decoded = jwt.verify(token, process.env.JWT_SECRET);
      // Assuming ownerId is same as decoded user id in the token
      if (decoded.userId !== ownerId) {
        throw new Error('Unauthorized: User is not allowed to create this book');
      }
    } catch (err) {
      throw new Error('Unauthorized: Invalid token');
    }

    const result = await db.query(
      'INSERT INTO Books (title, author, genre, ownerId) VALUES ($1, $2, $3, $4) RETURNING *',
      [title, author, genre, ownerId]
    );
    return result.rows[0];
  },

  async updateBookAvailability(bookId, available, token) {
    // Verify JWT token
    try {
      const decoded = jwt.verify(token, process.env.JWT_SECRET);
      // Check if the user is authorized to update the book
      // For example, check if the user is the owner of the book
      // You may need additional logic here based on your application requirements
    } catch (err) {
      throw new Error('Unauthorized: Invalid token');
    }

    const result = await db.query(
      'UPDATE Books SET available = $1 WHERE bookId = $2 RETURNING *',
      [available, bookId]
    );
    return result.rows[0];
  },
};

module.exports = Book;
