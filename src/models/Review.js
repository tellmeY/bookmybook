const jwt = require('jsonwebtoken');
const db = require('../config/database');

const Review = {
  async getAllReviews() {
    const result = await db.query('SELECT * FROM Reviews');
    return result.rows;
  },

  async getReviewById(reviewId) {
    const result = await db.query('SELECT * FROM Reviews WHERE reviewId = $1', [reviewId]);
    return result.rows[0];
  },

  async createReview(bookId, userId, rating, review, token) {
    // Verify JWT token
    try {
      const decoded = jwt.verify(token, process.env.JWT_SECRET);
      // Additional verification logic if needed
    } catch (err) {
      throw new Error('Unauthorized: Invalid token');
    }

    const result = await db.query(
      'INSERT INTO Reviews (bookId, userId, rating, review) VALUES ($1, $2, $3, $4) RETURNING *',
      [bookId, userId, rating, review]
    );
    return result.rows[0];
  },
};

module.exports = Review;
