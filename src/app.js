require('dotenv').config(); // Load environment variables from .env file

const express = require('express');
const jwt = require('jsonwebtoken');
const app = express();
const port = process.env.PORT || 3000; // Use PORT environment variable or default to 3000

// Check if JWT_SECRET is set
if (!process.env.JWT_SECRET) {
  console.error('JWT_SECRET is not set in the .env file');
  process.exit(1); // Exit the application
}

// Import database configuration
const db = require('./config/database');

// Import routes
const userRoutes = require('./routes/userRoutes');
const bookRoutes = require('./routes/bookRoutes');
const loanRoutes = require('./routes/loanRoutes');
const reviewRoutes = require('./routes/reviewRoutes');
const wishlistRoutes = require('./routes/wishlistRoutes');

// Connect to the database
db.connect();

// Middleware to verify JWT token
const verifyToken = (req, res, next) => {
  const token = req.headers.authorization?.split(' ')[1]; // Extract token from "Bearer <token>"

  if (!token) {
    return res.status(401).json({ error: 'Unauthorized: No token provided' });
  }

  jwt.verify(token, process.env.JWT_SECRET, (err, decoded) => {
    if (err) {
      return res.status(403).json({ error: 'Unauthorized: Invalid token' });
    }

    req.user = decoded; // Attach the decoded user data to the request object
    next();
  });
};

// Middleware to log JWT token (for development purposes)
app.use((req, res, next) => {
  const token = req.headers.authorization?.split(' ')[1];
  console.log('JWT Token:', token);
  next();
});

// Middleware
app.use(express.json());

// Routes
app.use('/users', userRoutes);
app.use('/books', verifyToken, bookRoutes); // Protect book routes
app.use('/loans', verifyToken, loanRoutes); // Protect loan routes
app.use('/reviews', verifyToken, reviewRoutes); // Protect review routes
app.use('/wishlist', verifyToken, wishlistRoutes); // Protect wishlist routes

// Start the server
app.listen(port, () => {
  console.log(`Server is running on port ${port}`);
});

// Close the database connection when the app exits
process.on('exit', db.end);
