const { Client } = require('pg');

const client = new Client({
  user: process.env.DB_USER,
  password: process.env.DB_PASSWORD,
  host: process.env.DB_HOST,
  port: process.env.DB_PORT,
  database: process.env.DB_NAME
});

module.exports = {
  connect: () => {
    client.connect()
      .then(() => console.log('Connected to the database'))
      .catch(err => console.error('Error connecting to the database', err.stack));
  },
  query: (text, params) => client.query(text, params),
  end: () => client.end()
};
