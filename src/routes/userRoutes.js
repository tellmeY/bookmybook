const express = require('express');
const router = express.Router();
const userController = require('../controllers/userController');
const jwtMiddleware = require('../middlewares/jwtMiddleware');

// Public routes
router.post('/register', userController.createUser);
router.post('/login', userController.loginUser);

// Protected routes
router.get('/', jwtMiddleware.verifyToken, userController.getAllUsers);
router.put('/:userId/reputation', jwtMiddleware.verifyToken, userController.updateUserReputation);

module.exports = router;
