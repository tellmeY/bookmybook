// bookRoutes.js
const express = require('express');
const router = express.Router();
const bookController = require('../controllers/bookController');

router.get('/', bookController.getAllBooks);
router.post('/', bookController.createBook);
router.put('/:bookId/availability', bookController.updateBookAvailability);

module.exports = router;
