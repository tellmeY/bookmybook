// loanRoutes.js
const express = require('express');
const router = express.Router();
const loanController = require('../controllers/loanController');

router.get('/', loanController.getAllLoans);
router.post('/', loanController.createLoan);
router.put('/:loanId/return', loanController.markLoanReturned);

module.exports = router;
