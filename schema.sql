-- Users table
CREATE TABLE Users (
  userId SERIAL PRIMARY KEY,
  username VARCHAR(50) NOT NULL UNIQUE,
  email VARCHAR(100) NOT NULL UNIQUE,
  password VARCHAR(100) NOT NULL,
  location VARCHAR(100),
  reputation_score FLOAT DEFAULT 0
);

-- Books table
CREATE TABLE Books (
  bookId SERIAL PRIMARY KEY,
  title VARCHAR(100) NOT NULL,
  author VARCHAR(100) NOT NULL,
  genre VARCHAR(50),
  ownerId INT NOT NULL,
  available BOOLEAN DEFAULT TRUE,
  FOREIGN KEY (ownerId) REFERENCES Users(userId)
);

-- Loans table
CREATE TABLE Loans (
  loanId SERIAL PRIMARY KEY,
  bookId INT NOT NULL,
  borrowerId INT NOT NULL,
  lenderId INT NOT NULL,
  loanStartDate DATE NOT NULL,
  dueDate DATE NOT NULL,
  returned BOOLEAN DEFAULT FALSE,
  FOREIGN KEY (bookId) REFERENCES Books(bookId),
  FOREIGN KEY (borrowerId) REFERENCES Users(userId),
  FOREIGN KEY (lenderId) REFERENCES Users(userId)
);

-- Reviews table
CREATE TABLE Reviews (
  reviewId SERIAL PRIMARY KEY,
  bookId INT NOT NULL,
  userId INT NOT NULL,
  rating INT CHECK (rating >= 1 AND rating <= 5),
  review TEXT,
  reviewDate DATE DEFAULT CURRENT_DATE,
  FOREIGN KEY (bookId) REFERENCES Books(bookId),
  FOREIGN KEY (userId) REFERENCES Users(userId)
);

-- Wishlist table
CREATE TABLE Wishlist (
  wishlistId SERIAL PRIMARY KEY,
  userId INT NOT NULL,
  bookId INT NOT NULL,
  FOREIGN KEY (userId) REFERENCES Users(userId),
  FOREIGN KEY (bookId) REFERENCES Books(bookId)
);
