# Use the official PostgreSQL image
FROM postgres:latest

# Set environment variables for PostgreSQL
ENV POSTGRES_USER=library_user
ENV POSTGRES_PASSWORD=your_password
ENV POSTGRES_DB=library_db

# Copy the SQL script containing the schema
COPY schema.sql /docker-entrypoint-initdb.d/

# Expose the default PostgreSQL port
EXPOSE 5432
